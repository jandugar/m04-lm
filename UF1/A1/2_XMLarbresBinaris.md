PRÀCTICA 2 - ARBRES BINARIS

![](img/2.1.png)
```
<?xml version="1.0" encoding="UTF-8"?>
<bookstore>
<book category="cooking">
 <title lang="en">Everyday Italian</title>
 <author>Giada De Laurentiis</author>
 <year>2005</year>
 <price>30.00</price>
</book>
<book category="children">
 <title lang="en">Harry Potter</title>
 <author>J K. Rowling</author>
 <year>2005</year>
 <price>29.99</price>
</book>
<book category="web">
 <title lang="en">Learning XML</title>
 <author>Erik T. Ray</author>
 <year>2003</year>
 <price>39.95</price>
</book>
</bookstore>
```
2.- Representar l'estructura d'arbre i escriure un document XML que
representi la següent informació sobre la carta del menú d'esmorzars
d'un restaurant :
![](img/2.2.a.png)
![](img/2.2.b.png)

3.- Representar l'estructura d'arbre i escriure un document XML que representi la següent informació :

![](img/2.3.a.png)
![](img/2.3.b.png)


4.- Escriure un document XML per guardar la següent informació sobre arbres:  


Acer monspessulanum
- Nombre común: Arce de Montpellier, Arce menor
- Vegetación: Caducifolio
- Altura: De 6 a 10 metros
- Forma y estructura: Copa esférica. Tronco principal recto con bifurcaciones. Ramaje colgante
- Color en primavera: Haz verde brillante, envés verde blanquecino
- Resistencia a las heladas: Heladas fuertes (hasta -15ºC)
- Otros: Las hojas se utilizan como infusión

Olea europea
- Nombre común: Olivo
- Vegetación: Perenne
- Altura: De 8 a 15 metros
- Forma y estructura: Copa irregular. Tronco principal irregular con bifurcaciones.cRamaje tortuoso
- Color en primavera: Haz verde oscuro, envés verde plateado
- Resistencia a las heladas: Heladas medias (hasta -10ºC)

Platanus orientalis
- Nombre común: Platano
- Vegetación: Caducifolio
- Altura: De 20 a 25 metros
- Forma y estructura: Copa ovoidal. Tronco principal recto. Ramaje expandido
- Color en primavera: Haz verde medio, enves verde claro
- Color en otoño: Ocre
- Resistencia a las heladas: Heladas fuertes (hasta -20ºC)

Quercus ilex
- Nombre común: Encina
- Vegetación: Perenne
- Altura: En torno a 25 metros
- Forma y estructura: Copa esférica o elíptica irregular. Tronco principal recto. Ramaje tortuoso
- Resistencia a las heladas: Heladas fuertes (hasta -15ºC)

5.- Escriure un document XML per guardar la següent factura d'una empresa de productes informàtics:

![](img/2.5.png)
