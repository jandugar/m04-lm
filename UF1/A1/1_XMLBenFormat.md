## PRÀCTICA 1: XML BEN FORMAT

### Well Formed XML Documents

#### An XML document with correct syntax is called "Well Formed".

Syntax rules:
-   XML documents must have a root element
-   XML elements must have a closing tag
-   XML tags are case sensitive
-   XML elements must be properly nested
-   XML attribute values must be quoted

Mira els següents exemples:
```
<arrel>
< hola>Incorrecte</hola;>
<hola;>Incorrecte</hola;>
<.hola>Incorrecte</.hola>
<:hola>Incorrecte</:hola>
<1hola>Incorrecte</1hola>
<hol.a>Correcte pero no recomanable</hol.a>
<hol:a>Gens recomanable</hol:a>
<xml-el\_que\_sigui>Incorrecte</xml-el\_que\_sigui>
<el\_què\_sigui-xml>Correcte pero no
recomanable<el\_què\_sigui-xml>
<hola=hola>Incorrecte</hola=hola>
</arrel>
```
1. Son correctes els “tags” següents?
```
<etiqueta1>
< etiqueta>
<etiqueta 1>
<etiqueta >
</etiqueta>
</etiqueta1 >
< /etiqueta2>
</etiqueta2>
<étiqueta>
```
2. Implementa un fitxer xml el següent text de manera que es pugui buscar informació segons els
següents camps: destinatari de la comanda, article demanat, direcció entrega, data d'entrega.
\

>Comanda pel senyor Joan Delgado Bruc. La comanda es composa d'una
bicicleta A2023. S'ha d'entregar al carrer Vent 4, tercer pis, lletra A, de Barcelona, el dia 19-10-2017.

3. Els següents documents estan ben formats?. Corregeix, si cal, els errors i escriu la solució.

3.1. Pel·lícules
```
<?xml version="1.0" encoding="UTF-8"?>
<pelicula>
 <titulo>Con faldas y a lo loco</titulo>
 <director>Billy Wilder</director>
</pelicula>
<pelicula>
 <director>Leo McCarey</director>
 <titulo>Sopa de ganso</titulo>
</pelicula>
<autor />barto</autor>
```
3.2. Programes
```
<?xml version="1.0" encoding="UTF-8"?>
<programas>
 <programa nombre="Firefox" licencia="GPL" licencia="MPL" />
 <programa nombre="OpenOffice.org" licencia=LGPL />
 <programa nombre="Inkscape" licencia="GPL" />
</programas>
```
3.3. Mundials de futbol
```
<?xml version="1.0" encoding="UTF-8"?>
<mundiales-de-futbol>
 <mundial>
 <pais="España" />
 <1982 />
 </mundial>
</mundiales-de-futbol>
```

4.- Està ben format el següent document? Enumera i explica, si cal, els errors existents.
```
<?xml version="1.0" encoding="UTF-8"?>
<deportistas>
 <deportista>
 <deporte Atletismo />
 <nombre>Jesse Owens</nombre>
 <deportista>
 <deporte Natación />
 <nombre>Mark Spitz</nombre>
 </deportista>
</deportistas>
```
