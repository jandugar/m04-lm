
![Logo](logo.png)


[MP4](MP4.md)
===============

-   [**UF1**: Programació amb XML](UF1)
    1.  [XML ben format](UF1/A1)
        -   [Crear fitxers de text](UF1/A1/T1.md)
        -   [Editar documents XML ben formats](UF1/A1/T2.md)
        -   [Verificar documents ben formats](UF1/A1/T3.md)
        -   [Definir i incloure entitats](UF1/A1/T4.md)
        -   ~~[Combinar documents XML amb XInclude](UF1/A1/T5.md)~~
        -   ~~[PE1: Conceptes fonamentals dels documents XML](UF1/A1/PE1.md)~~
        -   ~~[EP1: Creació de documents XML](UF1/A1/EP1.md)~~
    2.  [XML vàlid](UF1/A2)
        -   [Dissenyar nous DTDs](UF1/A2/T1.md) [`PDF`](UF1/A2/03_DTDs.pdf)
            -   [Exercicis 3](UF1/A2/3_XML_DTD.pdf)
            -   [Exercicis 3 bis](UF1/A2/3B_XML_DTDs_extra.md)
        -   [Validar documents XML](UF1/A2/T2.md)
        -   [Organitzar i documentar DTDs](UF1/A2/T3.md)
        -   [Espais de nom (PDF)](UF1/A2/04_Namespaces.pdf)
            -   [Exercici 4](UF1/A2/4_Namespaces.md)
        -   [Esquemes (PDF)](UF1/A2/05_XMLSchema.pdf)
            -   [Exercicis](UF1/A2/5_Esquemes.md)
        -   ~~[PE1: Conceptes bàsics dels DTDs](UF1/A2/PE1.md)~~
        -   ~~[EP1: Creació de nous DTDs](UF1/A2/EP1.md)~~
    3.  [El vocabulari HTML 4](UF1/A3)
        -   [Crear i validar documents XHTML](UF1/A3/T1.md)
        -   [Utilitzar documentació interactiva](UF1/A3/T2.md)
        -   [Convertir documents HTML a XHTML](UF1/A3/T3.md)
        -   [Editar XHTML amb un editor estructurat](UF1/A3/T4.md)
        -   [Aplicar fulls d’estil a documents XHTML](UF1/A3/T5.md)
        -   [PE1: Conceptes fonamentals del vocabulari XHTML](UF1/A3/PE1.md)
        -   [EP1: Creació de documents XHTML estricte](UF1/A3/EP1.md)
-   [**UF2**: Àmbits d’aplicació de l’XML](UF2)
    1.  ~~[La família de vocabularis RSS](UF2/A1)~~
        -   [Utilitzar lectors i agregadors RSS](UF2/A1/T1.md)
        -   [Crear i validar documents RSS](UF2/A1/T2.md)
        -   [Aplicar fulls d’estil a documents RSS](UF2/A1/T3.md)
        -   [PE1: Conceptes fonamentals del vocabulari RSS](UF2/A1/PE1.md)
        -   [EP1: Creació i validació de documents RSS](UF2/A1/EP1.md)
    2.  [Sistemes de consulta i emmagatzematge de documents XML: `Xpath`](UF/2A2)
        -   [Consultar documents XML](UF2/A2/T1.md)
        -   [Comprimir documents XML](UF2/A2/T2.md)
        -   [Extreure dades de SGBD relacionals en format XML](UF2/A2/T3.md)
        -   [PE1: Tècniques de programació declarativa amb Python](UF2/A2/PE1.md)
        -   [EP1: Interrogació de documents XML](UF2/A2/EP1.md)
    3.  [Transformació de documents XML](MP4.md#acMP4UF2A3)
        -   [Transformar documents XML](UF2/A3/T1.md)
        -   [Organitzar i documentar les transformacions](UF2/A3/T2.md)
        -   [PE1: Característiques del mòdul ElementTree de Python](UF2/A3/PE1.md)
        -   [EP1: Programació de transformacions](UF2/A3/EP1.md)
-   [**UF3**: Sistemes empresarials de gestió d’informació](UF3)
    1.  [Formularis md interactius](MP4.md#acMP4UF3A1)
        -   [Crear formularis](UF3/A1/T1.md)
        -   [Presentar formularis i dades utilitzant taules](UF3/A1/T2.md)
        -   [Processar formularis](UF3/A1/T3.md)
        -   [PE1: Conceptes bàsics dels formularis HTML](UF3/A1/PE1.md)
        -   [EP1: Creació de formularis](UF3/A1/EP1.md)
        -   [EP2: Processament de formularis](UF3/A1/EP2.md)

------------------------------------------------------------------------
